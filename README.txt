Accessibility Filter

This module combines several filters for accessibility and/or cleaning HTML code. Install it via composer, or just like any other module. The filters consist of:

Converting bold tags to strong tags
Converting underline tags to styled span tags
Converting italics tags to em tags
Replacing nbsps with blank spaces
Converting quotes to plain quotes
Converting apostrophes to plain apostrophes

There are no requirements aside from Drupal Core.