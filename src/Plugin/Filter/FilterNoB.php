<?php

namespace Drupal\accessibility_filter\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * Replaces all b tags with strong tags
 *
 * @Filter(
 *   id = "filter_no_b",
 *   module = "accessibility_filter",
 *   title = @Translation("Accessibility Bold Tags Filter"),
 *   description = @Translation("Selecting this turns all b tags into strong tags."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterNoB extends FilterBase {

  /**
   * {@inheritdoc}
   */
  
  public function process($text, $langcode) {
    return new FilterProcessResult(
      _no_b_eraser(
        $text,
      )
    );
  }

  /**
   * {@inheritdoc}
   */
}
