<?php

namespace Drupal\accessibility_filter\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * Replace i tags with em tags.
 *
 * @Filter(
 *   id = "filter_no_i",
 *   module = "accessibility_filter",
 *   title = @Translation("Accessibility Italics Filter"),
 *   description = @Translation("Selecting this turns all i tags into em tags."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterNoI extends FilterBase {

  /**
   * {@inheritdoc}
   */

  public function process($text, $langcode) {
    return new FilterProcessResult(
      _no_i_eraser(
        $text,
      )
    );
  }

  /**
   * {@inheritdoc}
   */
}
