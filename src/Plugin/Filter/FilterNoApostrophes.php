<?php

namespace Drupal\accessibility_filter\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * Replace all apostrophes with plain apostrophes.
 *
 * @Filter(
 *   id = "filter_no_apostrophes",
 *   module = "accessibility_filter",
 *   title = @Translation("Plain Apostrophes Filter"),
 *   description = @Translation("Selecting this turns all apostrophes into plain apostrophes."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterNoApostrophes extends FilterBase {

  /**
   * {@inheritdoc}
   */

  public function process($text, $langcode) {
    return new FilterProcessResult(
      _no_apostrophes_eraser(
        $text,
      )
    );
  }

  /**
   * {@inheritdoc}
   */
}
