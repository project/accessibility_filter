<?php

namespace Drupal\accessibility_filter\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * Replace all u tags with styled span tags.
 *
 * @Filter(
 *   id = "filter_no_u",
 *   module = "accessibility_filter",
 *   title = @Translation("Accessibility Underline Filter"),
 *   description = @Translation("Selecting this turns all u tags into span tags styled to be underlined."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterNoU extends FilterBase {

  /**
   * {@inheritdoc}
   */

  public function process($text, $langcode) {
    return new FilterProcessResult(
      _no_u_eraser(
        $text,
      )
    );
  }

  /**
   * {@inheritdoc}
   */
}
