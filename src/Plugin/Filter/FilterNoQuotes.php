<?php

namespace Drupal\accessibility_filter\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * Replaces all quotes with plain quotes.
 *
 * @Filter(
 *   id = "filter_no_quotes",
 *   module = "accessibility_filter",
 *   title = @Translation("Replace Quotes Filter"),
 *   description = @Translation("Selecting this turns all left and right quotes into plain quotes."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterNoQuotes extends FilterBase {

  /**
   * {@inheritdoc}
   */

  public function process($text, $langcode) {
    return new FilterProcessResult(
      _no_quotes_eraser(
        $text,
      )
    );
  }

  /**
   * {@inheritdoc}
   */
}
