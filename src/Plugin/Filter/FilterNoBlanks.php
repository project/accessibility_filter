<?php

namespace Drupal\accessibility_filter\Plugin\Filter;

use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
use Drupal\Core\Form\FormStateInterface;

/**
 * Replcaes all nbsps with blank spaces.
 *
 * @Filter(
 *   id = "filter_no_blanks",
 *   module = "accessibility_filter",
 *   title = @Translation("Remove nbsps Filter"),
 *   description = @Translation("Selecting this will turn all nbsps into blank spaces."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterNoBlanks extends FilterBase {

  /**
   * {@inheritdoc}
   */

  public function process($text, $langcode) {
    return new FilterProcessResult(
      _no_blanks_eraser(
        $text,
      )
    );
  }

  /**
   * {@inheritdoc}
   */
}
