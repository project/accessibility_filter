<?php

namespace Drupal\no_u\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'no_u' formatter.
 *
 * @FieldFormatter(
 *   id = "no_u",
 *   label = @Translation("Accessibility Underline Filter"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class NoUFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#markup' => _no_u_eraser($item->value),
      ];
    }
	
    return $element;
  }

}
