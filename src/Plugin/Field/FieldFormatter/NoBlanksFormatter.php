<?php

namespace Drupal\no_blanks\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'no_blanks' formatter.
 *
 * @FieldFormatter(
 *   id = "no_blanks",
 *   label = @Translation("Remove &nbsps Filter"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class NoBlanksFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#markup' => _no_blanks_eraser($item->value),
      ];
    }

    return $element;
  }

}
