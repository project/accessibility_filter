<?php

namespace Drupal\no_apostrophes\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'no_apostrophes' formatter.
 *
 * @FieldFormatter(
 *   id = "no_apostrophes",
 *   label = @Translation("Plain Apostrophes Filter"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class NoApostrophesFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#markup' => _no_apostrophes_eraser($item->value),
      ];
    }
	
    return $element;
  }

}
