<?php

namespace Drupal\no_quotes\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'no_quotes' formatter.
 *
 * @FieldFormatter(
 *   id = "no_quotes",
 *   label = @Translation("Replace Quotes Filter"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class NoQuotesFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#markup' => _no_quotes_eraser($item->value),
      ];
    }
	
    return $element;
  }

}
